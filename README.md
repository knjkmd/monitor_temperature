# monitor_temperature
The purpose of this project is to monitor temparature and humidity inside a house using esp32.

# Components of the project
## Physical devices
* Esp32: Micro computer
* DHT22: Temparature sensor

## Software components
* Eclipse Mosquitto (Docker): MQTT broker for publish and subscribe temperature data
* InfluxDB: Database for temparture data
* Grafana (Docker): Data visualization for temparture
* Python mqtt subscriber: Script to populate subscribed data to the database

## Extra software components
* Python open weather map client: Script to get weather information of different cities using open weather map api and populate the data to the database. 


# Eclipse Mosquitto (Docker)
A mosquitto broker runs on my vpn server using Docker. This has been implemented using Ansible playbook.

References:
https://hub.docker.com/_/eclipse-mosquitto


# InfluxDB
Influx DB is running on the desktop machine at home. It has been installed locally and the access from outside of the network is forwared to the influx db port.

* TODO: Check installed version 
* TODO: Invesigate possibility to replace by docker


# Grafana (Docker)
Grafana runs on the desktop machine at home using Docker. Currently, it is not accessble from outside of the network. The command to run Grafana using Docker is 
```
docker run -d -p 3000:3000 grafana/grafana
```
After running Grafana, data source has been configured from the Web UI, `fqdn:3000`.

* TODO: Run Grafana on the vpn server so that it's accessible from anywhere.


# Python mqtt subscriber


# Python open weather map client
